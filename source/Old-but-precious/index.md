# Old and Precious

Here is some old presentations and tutorials:

You can also look through the following presentations to get a first overview of **the TANGO Control
 system**.

 - {download}`TANGO introduction <tango_introduction.pdf>`
 - {download}`Overview of the Tango control system <tangooverview.pdf>`
 - {download}`TANGO basics, technical overview <tango_basics.pdf>`

You can check some detailled documentation:

```{toctree}
:maxdepth: 2

scada_introduction
training_C-plus-plus
training_Python
```
