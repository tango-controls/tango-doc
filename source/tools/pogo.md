(pogo-documentation)=

# POGO

```{tags} audience:developers, lang:c++, lang:java, lang:python
```
%[glossary_term][Pogo]
%Java tool for generating boiler plate code for C++/Java Tango Device servers.

- Pogo is the [TANGO] code generator written in Java swing.

- This graphical interface allows to define a [TANGO] class model.

- This class model will be saved in a **.xmi** file.

- From this [TANGO] class model Pogo is able to generate:
  : - a device server in C++, Java or Python.
    - An *html* documentation based on information entered during class model creation.

- The code generation part is based on [EMF] (Eclipse Model Framework) associated with  [Xtext] and [Xtend]  classes.

- Requirement: This tool needs java 1.7 or higher to be able to run.

- Source:

  > project: <https://gitlab.com/tango-controls/pogo>
  >
  > download: <https://bintray.com/tango-controls/maven/Pogo/_latestVersion>

[emf]: http://www.eclipse.org/
[tango]: http://www.tango-controls.org/
[xtend]: http://www.eclipse.org/Xtend
[xtext]: http://www.eclipse.org/Xtext
