(itango-tool)=

# ITango

ITango is a PyTango CLI based on [IPython](https://ipython.org). It is designed to be used as an IPython profile.

See [ITango documentation](inv:itango:std#index).
