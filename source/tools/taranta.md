
(taranta-documentation)=

# Taranta

```{tags} audience:all, lang:javascript
```

Taranta (Webjive until v.1.1.5) is a web application that allows a user to create a graphical user interface to interact with Tango devices. The interface may include a variety of charts, numerical indicators, dials, commands that can be used to monitor and to control devices. Very little knowledge of web technologies is needed.

With Taranta you can use your web browser to:
- View a list of all Tango devices
- View and modify device properties
- View and execute device commands
- Create dashboards for interacting with Tango devices.

See [Taranta documentation](inv:taranta:std#index)
