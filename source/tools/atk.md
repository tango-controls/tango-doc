(atk-documentation)=

# Tango Application Toolkit “ATK”

```{tags} audience:developers, lang:java
```

Tango Application ToolKit, (ATK) is a TANGO client framework for building GUIs based on Java Swing.

Speeding up the development and standardizing look and feels ATK provides several swing based components
to view and/or to interact with Tango device attributes and Tango device commands and also a complete synoptic viewing system.

Implementing the core of “any” Tango application ATK takes in charge the automatic update of device data either through Tango events or
by polling the device attributes. ATK takes also in charge the error handling and display.

The ATK swing components are the Java Beans, so they can easily be added to a Java IDE (like NetBeans) to speed up the development of graphical control applications.

ATK is composed of two jarfiles ATKCore.jar and ATKWidget.jar. You can download them from the [ATK release page](https://gitlab.com/tango-controls/atk/-/releases).
