# History

```{tags} audience:all
```

The concept of using Devices embedded in Device servers to implement access to devices in a control system was first proposed at the [ESRF](https://www.esrf.eu) in 1989.
The concept was implemented in the TACO control system which was presented at the ICALEPCS conference in 1991 ([Object Oriented Programming Techniques Applied to Device Access and Control](https://doi.org/10.18429/JACoW-ICALEPCS1991-S14OOP04)).
It has been successfully used as the heart of the ESRF Control System of the institute accelerator complex.
This control system, called TACO, was based on the SUN RPC (also used by the NFS protocol) and C as its core programming language.

In 1999, a renewal of the ESRF distributed control system was started with the aim of replacing SUN/RPC with CORBA, using C++ as the core programming languages.
The new software was called TANGO and was presented for the first time at ICALEPCS in 1999 ([TANGO - AN OBJECT ORIENTED CONTROL SYSTEM BASED ON CORBA](https://accelconf.web.cern.ch/ica99/papers/wa2i01.pdf))

In June 2002, Soleil and ESRF officially decided to collaborate to develop TANGO collaboratively.
[Soleil](https://www.synchrotron-soleil.fr/) is the French synchrotron radiation facility based close to Paris.
In December 2003, Elettra joined the club.
[Elettra](https://www.elettra.trieste.it/) is an Italian synchrotron radiation facility located in Trieste.
Beginning of 2005 ALBA also decided to join.
[ALBA](https://www.cells.es/en/) is a Spanish synchrotron radiation facility located in Barcelona.
[DESY](https://www.desy.de/) and [MaxIV](https://www.maxiv.lu.se/) were the next big synchrotrons in Europe to join the collaboration.
After that things speeded up and more and more sites doing diverse things developed collaboratively.
A number of  lasers, telescopes, fusion facilities have decided to use TANGO as their control system.
The most recent and largest facility to join is [SKA](https://www.skao.int/en), the Square Kilometer Array, which is constructing the two largest radio telescopes in the world.

In 2015 approximately TANGO was officially renamed to be **Tango Controls** and is referred to as **Tango** for short.
