# Authors

(authors)=

```{tags} audience:all
```

Good documentation needs dedicated authors who spend lots of time writing and reading text instead of code. This labour of love is only rarely appreciated by readers. This section lists the numerous contributors to the Tango documentation. If you are reading this section don't hesitate to send them some positive thoughts and thanks for their "labour of love" right now!

The Tango Controls documentation you are reading has been assembled into its current form at four documentation sprints and workshops:

1. **Third Write-the-Doc Team** - the following people participated in the third Write-the-Doc camp held at **l'Escandille** in Autrans (Vercors) in October 2024:
 Benjamin Bertrand, Reynald Bourtembourg, Thomas Braun, Andy Götz, Vincent Hardion, Anton Joubert, Thomas Jürges, Damien Lacoste, Vicente Rey-Bakaikoa, Nicolas Tappret

- **Second Write-the-Doc Team** - the following people participated in the second Write-the-Doc camp held at **Solaris** in Krakow (Poland):
  Reynald Bourtembourg, Thomas Braun, Sébastien Gara, Philippe Gauron, Andrew Götz, Piotr Goryl, Anton Joubert, Krystian Kędroń, Igor Khokhrakiov, Grzegorz Kowalski, Olga Merkulova, Guillaume Mugerin, Lorenzo Pivetta and Sergi Rubio

- **First Write-the-Doc Team** - the following people assisted to the first Write-the-Doc camp in **St Nizier du Moucherotte** (Vercors):
  Piotr Goryl, Olga Merkulova, Lukasz Zytniak, Lukasz Dudek, Matteo di Carlo, Matteo Canzari, Igor Khokhrakiov, Reynald Bourtembourg, Jean-Michel Chaize, Stuart James and Andy Götz

The following people have contributed to the Tango documentation over the years:

- **Gwenaelle Abeille** - for writing the original JTango documentation
- **Benjamin Bertrand** - for converting RST to MyST and managing the restructured documentation
- **Reynald Bourtembourg** - for writing the HDB++ documentation
- **Thomas Braun** - for doing the first conversion of the Book to Sphinx in RST and then again to MyST
- **Alain Buteau** - for the guidelines documentation
- **Matteo di Carlo** - for drawing the Device Server system model
- **Tiago Couthino** - for writing the PyTango documentation and showing the
  way with Sphinx
- **Lukasz Dudek** - for converting many documents to Sphinx and setting up CI
- **Philippe Gauron** - for converting many documents to MyST
- **Piotr Goryl** - for reformatting the documentation into Sphinx and documentation master
- **Lajos Fülöp** - for the original layered maps
- **Andy Götz** - for contributing and motivating to have a complete Tango documentation
- **Vincent Hardion** - for converting to MyST and building the restructured documentation
- **Stuart James** - for editing and updating the layered maps
- **Thomas Juerges** - for writing explanations and being picky about thos ehe didn't write
- **Igor Khokhrakiov** - for writing the new version of JTango documentation, REST api, Amazon cloud etc
- **Damian Lacoste** - for converting documents to MyST and rewriting Pogo and HDB++ documentation
- **Nicolas Leclerq** - for the Yat4Tango, bindings documentations
- **Olga Merkulova** - for re-organising the documentation and writing getting started
- **Lorenzo Pivetta** - for writing the HDB++ documentation
- **Faranguiss Poncet** - for writing the ATK documentation
- **Jean-Luc Pons** - for writing the Jive documentation
- **Sergi Rubio** - for the Fandango and Panic documentation
- **Olivier Tachet** - for the how to on installing Tango on a Raspberry Pi
- **Emmanuel Taurel** - for writing the first Tango documentation (*The Book*) single handedly!
- **Pascal Verdier** - for writing the Pogo and Astor documentation
- **Rebecca Williams** - for the huge job restructing the documentation to follow the [GUTD](https://docs.divio.com/documentation-system/)
- **Lukasz Zytniak** - for converting many documents to Sphinx

Last but not least :
- **Guidelines Team** - the following people contributed to the
  {doc}`device server guidelines <Tutorials/development/device-api/ds-guideline/index>`:
  Alain Buteau, Jens Meyer, Jean Michel Chaize, Emmanuel Taurel, Pascal Verdier, Nicolas Leclerq,
 M.Lindberg, Sebastien Gara, S. Minolli, and Andy Götz.

Please add your name to the above list if you have contributed to the Tango Documentation.

A huge **Thank You** to all of you!

## Acknowledgements

The current Tango documentation would not be possible without the help of:

- **Sphinx** - a big thank you especially to Georg Brandl for inventing Sphinx (by chance Georg is also a member of the Tango community)
- **MyST** - for the simple but powerful markdown language
- **Github** - for hosting the tango-doc repository
- **Travis** - for the continuous integration of tango-doc
- **Read-the-docs** - for formatting and hosting the online documentation
- **Tango Collaboration** - who sponsored the first Tango Write-the-docs camp and the conversion to Sphinx
