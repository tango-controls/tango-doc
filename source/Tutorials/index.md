(tutorials-index)=

# Tutorials

```{tags} audience:all
```
In this section you will find a set of tutorials to get you started with using Tango.

- The [Getting started](#tutorial-getting-started) section is a good place to start with instructions on how to get a simple Tango environment up and running followed by a guide to creating a simple Tango device server and client to demonstrate how Tango works.

- There is an [Example deployment of a Tango Control System](#tutorial-deployment) highlighting the important components that need to be set up and the tasks that they perform. This gives a good overview of how the whole system fits together.

- A tutorial on how to use {term}`AtkPanel`s to control a Tango device is given in the [ATKPanel](#atkpanel-manual) subsection. This is a good example of how to interact with device servers using a client.

- Finally, a more in-depth tutorials on how to write Tango device servers and clients, including examples in all three languages; C++, Python and Java, can be found in the [Developer's Guide](#tutorial-developers) subsection.


```{toctree}
:maxdepth: 1
:name: Tutorials
:hidden:

getting-started/tutorial.md
installation-minimum-deployment.md
atkpanel/atkpanel
development/index.md
```
