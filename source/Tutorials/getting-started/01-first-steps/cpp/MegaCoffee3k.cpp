/*----- PROTECTED REGION ID(MegaCoffee3k.cpp) ENABLED START -----*/
/* clang-format on */
//=============================================================================
//
// file :        MegaCoffee3k.cpp
//
// description : C++ source for the MegaCoffee3k class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               MegaCoffee3k are implemented in this file.
//
// project :     Tango MegaCorp Coffee machines 3000 series
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include "MegaCoffee3k.h"
#include "MegaCoffee3kClass.h"
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k.cpp

/**
 *  MegaCoffee3k class description:
 *
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  Inherited (no method)
//  Status        |  Inherited (no method)
//================================================================

//================================================================
//  Attributes managed is:
//================================================================
//================================================================

namespace MegaCoffee3k_ns
{
/*----- PROTECTED REGION ID(MegaCoffee3k::namespace_starting) ENABLED START -----*/
/* clang-format on */
//	static initializations
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::namespace_starting

//--------------------------------------------------------
/**
 *	Method     : MegaCoffee3k::MegaCoffee3k()
 *	Description: Constructors for a Tango device
 *                implementing the classMegaCoffee3k
 */
//--------------------------------------------------------
MegaCoffee3k::MegaCoffee3k(Tango::DeviceClass *cl, std::string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(MegaCoffee3k::constructor_1) ENABLED START -----*/
	/* clang-format on */
	init_device();
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::constructor_1
}
//--------------------------------------------------------
MegaCoffee3k::MegaCoffee3k(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(MegaCoffee3k::constructor_2) ENABLED START -----*/
	/* clang-format on */
	init_device();
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::constructor_2
}
//--------------------------------------------------------
MegaCoffee3k::MegaCoffee3k(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(MegaCoffee3k::constructor_3) ENABLED START -----*/
	/* clang-format on */
	init_device();
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::constructor_3
}
//--------------------------------------------------------
MegaCoffee3k::~MegaCoffee3k()
{
	delete_device();
}

//--------------------------------------------------------
/**
 *	Method     : MegaCoffee3k::delete_device()
 *	Description: will be called at device destruction or at init command
 */
//--------------------------------------------------------
void MegaCoffee3k::delete_device()
{
	DEBUG_STREAM << "MegaCoffee3k::delete_device() " << device_name << std::endl;
	/*----- PROTECTED REGION ID(MegaCoffee3k::delete_device) ENABLED START -----*/
	/* clang-format on */
	//	Delete device allocated objects
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::delete_device
}

//--------------------------------------------------------
/**
 *	Method     : MegaCoffee3k::init_device()
 *	Description: will be called at device initialization.
 */
//--------------------------------------------------------
void MegaCoffee3k::init_device()
{
	DEBUG_STREAM << "MegaCoffee3k::init_device() create device " << device_name << std::endl;
	/*----- PROTECTED REGION ID(MegaCoffee3k::init_device_before) ENABLED START -----*/
	/* clang-format on */
	//	Initialization before get_device_property() call
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::init_device_before

	//	No device property to be read from database

	/*----- PROTECTED REGION ID(MegaCoffee3k::init_device) ENABLED START -----*/
	/* clang-format on */
	//	Initialize device
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::init_device
}


//--------------------------------------------------------
/**
 *	Method     : MegaCoffee3k::always_executed_hook()
 *	Description: method always executed before any command is executed
 */
//--------------------------------------------------------
void MegaCoffee3k::always_executed_hook()
{
	DEBUG_STREAM << "MegaCoffee3k::always_executed_hook()  " << device_name << std::endl;
	/*----- PROTECTED REGION ID(MegaCoffee3k::always_executed_hook) ENABLED START -----*/
	/* clang-format on */
	//	code always executed before all requests
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method     : MegaCoffee3k::read_attr_hardware()
 *	Description: Hardware acquisition for attributes
 */
//--------------------------------------------------------
void MegaCoffee3k::read_attr_hardware(TANGO_UNUSED(std::vector<long> &attr_list))
{
	DEBUG_STREAM << "MegaCoffee3k::read_attr_hardware(std::vector<long> &attr_list) entering... " << std::endl;
	/*----- PROTECTED REGION ID(MegaCoffee3k::read_attr_hardware) ENABLED START -----*/
	/* clang-format on */
	//	Add your own code
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::read_attr_hardware
}


//--------------------------------------------------------
/**
 *	Method     : MegaCoffee3k::add_dynamic_attributes()
 *	Description: Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void MegaCoffee3k::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(MegaCoffee3k::add_dynamic_attributes) ENABLED START -----*/
	/* clang-format on */
	//	Add your own code to create and add dynamic attributes if any
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method     : MegaCoffee3k::add_dynamic_commands()
 *	Description: Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void MegaCoffee3k::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(MegaCoffee3k::add_dynamic_commands) ENABLED START -----*/
	/* clang-format on */
	//	Add your own code to create and add dynamic commands if any
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::add_dynamic_commands
}

/*----- PROTECTED REGION ID(MegaCoffee3k::namespace_ending) ENABLED START -----*/
/* clang-format on */
//	Additional Methods
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	MegaCoffee3k::namespace_ending
} //	namespace
