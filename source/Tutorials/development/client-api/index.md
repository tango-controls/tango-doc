(client-api)=

# Tango Client

```{tags} audience:developers, lang:all
```

```{toctree}
:maxdepth: 2
:name: clientapitoc

atk-programmers-guide
../device-api/python/index
```
