(device-api)=

# Device Servers

```{tags} audience:developers, lang:all
```

```{toctree}
:maxdepth: 1
:name: deviceapitoc

introduction
ds-guideline/index
device-server-model
device-server-writing
generating-events
java/index
python/index
```
