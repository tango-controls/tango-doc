# Tango Core: Python

## PyTango usage

For instructions on how to use PyTango, the Python binding for Tango Controls,
please refer to the [PyTango documentation](inv:pytango:std#index)

(pytango-api-docs)=
## PyTango API reference

Please refer to [PyTango API documentation](inv:pytango:std:doc#api)
