# Ecosystem

```{tags} audience:all
```

Tango Controls offers a rich ecosystem for developers and users alike.

Since Tango is a also a developer's framwork there are many libraries and tools for existing devices and clients. Refer to the [overview for developers](#developers-overview) for more information.

Tango is designed to run small and large systems. To support managing large installations a number of administration tools are provided.

% New link to find?
% The [overview for administrators](#administrators-overview) is a good starting point.

All control systems need to be able to archive data so that one can look at past data when needed. Tango comes with a number of archiving solutions and the [archiving overview](#archiving-overview) is our starting point for them.

In addition to the Python, C++ and Java APIs for Tango a number of other languages and tools can be used together with Tango. Please have a look at our [bindings overview](#bindings-overview) where we list the currently known ones.
