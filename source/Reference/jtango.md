# Tango Core:  Java

## JTango usage

For instructions on how to use JTango, the Java binding for Tango Controls,
please refer to the
[JTango documentation](https://jtango.readthedocs.io/en/latest/).

(jtango-api-docs)=
## JTango API reference

- [JTango Server](https://www.javadoc.io/doc/org.tango-controls/JTangoServer/9.7.2/index.html)
- [JTango Client](https://www.javadoc.io/doc/org.tango-controls/JTangoClientLang/9.7.2/index.html)
- [JTango Common](https://www.javadoc.io/doc/org.tango-controls/JTangoCommons/9.7.2/index.html)
