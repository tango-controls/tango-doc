# Matlab & Octave

(matlab-octave-binding)=

```{tags} audience:all
```

- Client API for [Matlab](https://www.mathworks.com/) and [Octave](https://www.gnu.org/software/octave/)
- Release 2.0.6 for Matlab >= R2009b or Octave >= 3.6.2
- Runs on Windows and Linux
- Provides 64 bits support on both platforms
  : - the 32 bits mode is still available on Linux but could be abandoned in a near future
- [Binary distribution 2.0.6](https://sourceforge.net/projects/tango-cs/files/bindings/tango-binding-2.0.6-matlab-windows-x86-msvc-10.zip/download) for Windows x86
  : - tested with Matlab R2009b
- [Binary distribution 3.1.0](https://sourceforge.net/projects/tango-cs/files/bindings/tango-binding-3.1.0-matlab-windows-x64-msvc-12.zip/download) for Windows x64
  : - tested with Matlab R2016b
    - this release contains a major change - see the [README](https://gitlab.com/tango-controls/matlab-binding/blob/master/README.md) file for details
- Source code available on [GitLab](https://gitlab.com/tango-controls/matlab-binding)
  : - please visit the [MathWorks web site](https://fr.mathworks.com/support/sysreq/previous_releases.html) in order to identify the official gcc version associated with your Matlab version
