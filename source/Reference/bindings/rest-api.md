# REST API

(rest-api-binding)=

```{tags} audience:all
```

- [Server implementations](https://gitlab.com/tango-controls/rest-api#known-server-implementations)
- [Client implementations](https://gitlab.com/tango-controls/rest-api#reference-client-implementations)

The API specification is discussed in [](tango-rest-api).
