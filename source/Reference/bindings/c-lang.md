# C Language

(c-language-binding)=

```{tags} audience:developers
```

C is supported for clients written in old style C (like [SPEC](https://certif.com/)). The C binding does not support all or the latest features of Tango. We strongly encourage everybody to use the C++ API instead because it will provide access to all features of Tango.

- C language
  : - A minimal client binding for the good old C language
    - Release 3.0.2 including features of Tango 8 but no events
    - [Source code](https://sourceforge.net/projects/tango-cs/files/bindings/c_binding_Release_3_0_2.tar.gz/download) distribution
