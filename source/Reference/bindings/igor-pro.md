# Igor Pro

(igor-pro-binding)=

```{tags} audience:all
```

- Client API for [Igor Pro](http://wavemetrics.com/)
- Release 3.0.0 for Igor Pro 7.x
  : - Runs on Windows x64 \[no official support for x86 any more\]
    - [Binary distribution for 7.x](https://sourceforge.net/projects/tango-cs/files/bindings/tango-binding-3.0.0-for-igor-pro-7-windows-x64.zip/download) for Windows x64
- Release 2.5.0 for Igor Pro 6.x
  : - Runs on Windows x86 & x64
    - [Binary distribution for 6.x](https://sourceforge.net/projects/tango-cs/files/bindings/tango-binding-2.5.0-3-igorpro6.2-win32-msvc-8.0.50727.762.zip/download) for Windows x86
- Source code available on [GitLab](https://gitlab.com/tango-controls/igorpro-binding)
  : - compiling this binding requires the [WaveMetrics' XOP Toolkit](http://www.wavemetrics.com/products/xoptoolkit/xoptoolkit.htm)
