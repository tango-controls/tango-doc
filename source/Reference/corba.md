# 10 things you should know about CORBA

```{tags} audience:developers, lang:all
```

01. **You don’t need to know CORBA to work with TANGO**
02. CORBA is the acronym for **C**ommon **O**bject **R**equest **B**roker **A**rchitecture and it is a standard defined by the [Object Management Group (OMG)](http://www.omg.org)
03. CORBA enables communication between software written in different languages and running on different computers
04. CORBA applications are composed of many objects; objects are running software that provides functionalities and that can represent something in the real world
05. Every object has a type which is defined with a language called IDL (Interface Definition Language)
06. An object has an interface and an implementation: this is the essence of CORBA because it allows *interoperability*.
07. CORBA allows an application to request an operation to be performed by a distributed object and for the results of the operation to be returned back to the application making the request.
08. CORBA is based on a *Remote Procedure Call* model
09. The TANGO Device is a CORBA Object
10. The TANGO Device Server is a CORBA Application
