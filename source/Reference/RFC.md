# RFC

(RFC)=

```{tags} audience:all
```

The [Tango Controls RFCs](https://tango-controls.readthedocs.io/projects/rfc/en/latest/README.html) (Request For Comments) are a collection of documents that contain the Tango Controls specification of concepts, terminology, Protocol behaviour and conventions. They are the definitive reference material and contain answers to all questions that are related to how Tango Controls is supposed to work.
