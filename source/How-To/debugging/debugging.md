(how-to-debugging)=
# Debugging and Testing

```{tags} audience:developers, lang:all
```

In the following articles you will find useful information on testing and debugging of your code.

One recommended way of testing newly developed Tango device servers is by using the available [Tango Docker containers](#tango-using-docker).

```{toctree}
:maxdepth: 2
:name: debuggingandtestingtoc
:hidden:

testing-tango-using-docker
how-to-junit-helper-classes-for-device-server-testing
```
