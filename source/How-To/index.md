(how-to-index)=

# How-Tos

```{tags} audience:all
```

This section contains a set of useful 'How tos' with instructions on how to perform some common tasks with Tango.

1. Getting started

2. For information on how to contribute to Tango including the IDL, documentation and source code in C++, Python and Java please see the [Contributing](#contributing) section

3. The [Debugging and Testing](#how-to-debugging) section provides instructions on how to use the Tango Docker containers to test newly developed Tango device servers.

4. The [Deployment](#how-to-deployment) section provides a set of how tos describing different ways to deploy and run Tango. This includes running with and without a DB, using access control and other available services.

```{toctree}
:maxdepth: 2
:name: How-Tos

installation/index
getting-started/index
deployment/index
development/index
debugging/debugging
contributing/contributing
```
