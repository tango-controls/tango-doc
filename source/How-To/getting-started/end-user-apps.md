(getting-started-as-user)=

# Use the end-user applications

```{tags} audience:all
```

End-users and administrators will be interested in how to use some of the tool provided by Tango to help interact with the control system. A full list of the tools delivered with Tango Controls can be found in the [tools](#tools-index) section.

Below, you will find a list of tools a beginner user usually needs to know.

## Jive

Jive is a tool used to configure components of the Tango Controls and browse a static {term}`Tango Database`. See the
[Jive Manual](inv:jive:std#index) for more information and examples of how to use it.

## ATKPanel

ATKPanel is a simple application which shows (and allows one to modify or invoke) device {term}`state <device state>`,
{term}`attributes <attribute>` and {term}`commands <command>`. Thus it allows one to test and control all devices in the system.
The tool is delivered together with Tango Controls. It may be opened as a stand-alone application or invoked from Jive.
See [ATKPanel Manual](#atkpanel-manual) for more information and examples of how to use it.

## LogViewer

Tango provides a logging facility and the [LogViewer application](#logviewer-manual) can be used to view such logs. This application is delivered with Tango Controls.


Administrators for Tango Controls may also be interested in the following tools:

## Astor

[Astor](#astor-manual) is a tool for management of Tango Controls system.


## Tango Database

[Tango Admin ](#tango-admin) is a command-line interface for Tango Database management.
