(how-to-deployment)=
# Deployment

```{tags} audience:administrators
```
This section provides a set of common how-to tasks relating to the deployment of Tango.

```{toctree}
:maxdepth: 1
:name: servicestoc
:caption: How to...

events
multiple-db-hosts
property-file
starter
filedatabase
starting
device-server-without-db
access-control
how-to-import-classes-to-catalogue
how-to-integrate-with-systemd
how-to-run-device-server-firewall
```
