(installation-vm)=

# Virtual Machine

```{tags} audience:administrators, audience:developers, audience:all
```

The purpose of the **TANGO Box Virtual Machine** is to give you a fast,
out-of-the-box experience of a working TANGO system.
There is a set of shortcuts to all essential TANGO tools on the virtual machine desktop.
Together with the  user documentation,
that allow you to experience the power and elegance of a fully configured
TANGO system with all the latest tools first hand. After this "guided tour" of the TANGO system,
TANGO Box is an excellent tool to make further explorations on your own,
to use it for demonstration purposes, to make studies,
proof-of-concepts and even production ready systems.

These images can also be used on cloud providers by converting them to the appropriate format.

## TangoBox 10.3

- You may download TangoBox 10.3 from
  [here](https://s2innovation.sharepoint.com/:u:/s/Developers/EXcBfXqgugRIkqP2GRgOycYB6_2xjxak7ukUrxwofcgo9A?e=wLChvo).

There is currently no up-to-date documentation.

## TangoBox 9.3

- You may download TangoBox 9.3 from
  [here](https://s2innovation.sharepoint.com/:f:/s/Developers/EovD2IBwhppAp-ZLXtawQ6gB9F6aXPPs2msr2hgPGTO-FQ?e=Ii3tnr).
- Please read {doc}`Tango Controls demo VM’s documentation <vm/tangobox>`.
- See also [a release note](https://gitlab.com/tango-controls/tangobox/releases/tag/v9.3.3)
  and [README](https://gitlab.com/tango-controls/tangobox/blob/develop/README.md).

### Minimum Requirements

- 2vCPU
- 2GB (preferred 4GB) RAM
- 30GB of disk space
