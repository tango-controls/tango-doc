(getting-started-installation)=

# Installation

```{tags} audience:administrators, audience:developers
```

Here you will find recipes on how to install the Tango Controls on various platforms. There are binary packages
available on all platforms to get you started quickly.

```{toctree}
:maxdepth: 2

conda.md
linux-debian
linux-redhat
tango-on-windows
tango-on-macos
virtualmachine
vm/tangobox.md
archiving/snap
```
