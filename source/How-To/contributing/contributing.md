(contributing)=
# Contributing

Instructions on how to contribute to the main projects within Tango can be found under this section. This include contributing to:

- The [Tango IDL](#how-to-contribute-idl): the CORBA IDL file
- [cppTango](#how-to-contribute-cpptango): the C++ library for Tango
- [pyTango](#how-to-contribute-pytango): the Python bindings for Tango
- [jTango](#how-to-contribute-jtango): the Java implementation of the Tango Kernel
- [Tango Documentation](#documentation-workflow-tutorial): this documentation of the Tango project


```{toctree}
:maxdepth: 1
:hidden:

tangoidl
cpptango
pytango
jtango
docs/docs
docs/how-to-add-subproject-to-the-doc
```
