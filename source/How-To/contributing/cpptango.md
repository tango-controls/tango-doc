(how-to-contribute-cpptango)=
# cppTango

For details on how to contribute to cppTango please see the [contributing page](https://gitlab.com/tango-controls/cppTango/-/blob/main/CONTRIBUTING.md) on the GitLab project.
