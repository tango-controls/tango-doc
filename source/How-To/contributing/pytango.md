(how-to-contribute-pytango)=
# PyTango

For information on how to contribute to PyTango, see the project's
[How to Contribute](inv:pytango:std:label#how-to-contribute) guide.
