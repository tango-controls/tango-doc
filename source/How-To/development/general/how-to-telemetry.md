(telemetry-howto)=

# Telemetry with Tango

cppTango and PyTango support telemetry via the
[OpenTelemetry](https://opentelemetry.io/docs/what-is-opentelemetry/) framework.

Please see their respective documentation for more instructions:

- [cppTango](https://tango-controls.gitlab.io/cppTango/10.0.0/telemetry.html)
- [PyTango](inv:pytango:std:label#telemetry-howto)
