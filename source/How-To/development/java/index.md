(getting-started-java)=

# Getting started with JTango (Java implementation of Tango-Controls)

```{tags} audience:developers, lang:java
```

```{toctree}
:name: gettingstartedtoc

jtango-client
jtango-server
how-to-integrate-java-server-with-astor.md
```

For a more complete guide on JTango please refer to the
[JTango documentation](https://jtango.readthedocs.io/en/latest/).
