# Getting started with PyTango (Python implementation of Tango-Controls)

```{tags} audience:developers, lang:python
```

```{toctree}
:maxdepth: 2

pytango-server
how-to-pytango
how-to-pytangoarchiving
```
