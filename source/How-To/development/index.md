(getting-started-as-developer)=

# Development

```{tags} audience:developers, lang:all
```

Here you will find recipies on how to develop with Tango Controls.

Some common task are given below:

```{toctree}
:maxdepth: 1
:caption: How to

general/how-to-first-device-class
general/first-client
general/generating-events
general/how-tune-polling-code-tango-class
general/how-to-telemetry
general/transferring-images

```

For language specific how-tos please see the following subsections:

```{toctree}
:maxdepth: 1
:caption: How to

cpp/index
java/index
python/index

```
