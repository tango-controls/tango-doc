# Get started with cppTango

```{tags} audience:developers, lang:c++
```
cppTango is the C++ implementation of Tango Controls. Below you will find some useful instructions on how to get start developing in C++.

Note that this chapter assumes that you have already installed Tango in your local computer or in your network. If you need to install Tango, please reference to [the documentation](#getting-started-installation).

```{toctree}
:caption: How to:
:maxdepth: 1


cpp-quick-start
first-device-class
user-loop
how-to-create-inheritance-link
use-vectors-set-attributes
how-to-dynamic-attributes-device-class
how-to-deal-string-tango-attribute-cpp
how-to-device-server-startup-time
how-to-enumerated-attribute
how-to-forwarded-attribute
how-to-memorised-attribute
how-to-aliases
how-to-cpp-client-programmers-guide
```
